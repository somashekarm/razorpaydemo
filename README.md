# RazorPay Demo Application
This repository contains the source code of a RazorPay Demo Application developed using android on Windows 10 System. For demo purpose NetBanking Payment method is performed by providing dummy mobile number and email address see in the result section below.

## Setup
Step 1. [Razorpay account creation](https://razorpay.com/docs/payment-gateway/dashboard-guide/sign-up/).

Step 2. For Test Mode complete these:

* Sign up

* Complete the pre-sign up form

* Verify email address

* No need for Account Activation for test mode.

Step 3: Import the RazorPayDemo application in your android studio.

Step 4. [Razorpay integration in android Application](https://razorpay.com/docs/payment-gateway/android-integration/standard/) is completed.

Step 5. Add your Razorpay API key to the AndroidManifest.xml
```
<meta-data
         android:name="com.razorpay.ApiKey"
         android:value="YOUR_KEY_ID"
         />
```
Step 6: Run the application

## Result

![SMainActivity UI](images/MainActivity.png) ![PhoneEmailEmpty UI](images/PhoneEmailEmpty.png) ![PhoneEmailFilled UI](images/PhoneEmailFilled.png)

![PaymentOptions UI](images/PaymentOptions.png) ![NetBankingSelection UI](images/NetBankingSelection.png) ![NetBankingSelected UI](images/NetBankingSelected.png)

![PaymentBankPage UI](images/PaymentBankPage.png) ![Successful UI](images/Successful.png)

![PaymentBankPage UI](images/PaymentBankPage.png) ![failure UI](images/failure.png) ![failure1 UI](images/failure1.png)
